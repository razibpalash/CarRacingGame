﻿using UnityEngine;
using System.Collections;

public class carSpawner : MonoBehaviour {

	public GameObject[] cars;
	int carNo;
	float maxPos = 2.09f;
	float minPos = -2.18f;
	public float delayTimer;
	float timer;

	// Use this for initialization
	void Start () {
		timer = delayTimer;

	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		if (timer <= 0) {
			Vector3 carPos = new Vector3 (Random.Range (minPos, maxPos), transform.position.y, transform.position.z);
			carNo = Random.Range (0, 5);
			Instantiate (cars[carNo], carPos, transform.rotation);	
			timer = delayTimer; 
		}
	
	}
}
