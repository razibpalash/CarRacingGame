﻿using UnityEngine;
using System.Collections;

public class CarController : MonoBehaviour {
	public float carSpeed;
	float maxPos = 2.09f;
	float minPos = -2.18f;
	Vector3 position;

	public UIManager ui;

	public AudioManager am;

	bool currentPlatformAndroid = false;
	Rigidbody2D rb;

	void Awake(){

		rb = GetComponent<Rigidbody2D> ();

		#if UNITY_ANDROID
			currentPlatformAndroid = true;
		#else
			currentPlatformAndroid = false;
		#endif
			
		am.carSound.Play();
	}

	// Use this for initialization
	void Start () {
		//am.carSound.Play ();
		position = transform.position;
		if (currentPlatformAndroid == true) {
			Debug.Log ("Android");
		} else {
			Debug.Log ("Windows");
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (currentPlatformAndroid == true) {
			TouchMove ();
			//AccelerometerMove();
			
		} else {
			position.x += Input.GetAxis ("Horizontal") * carSpeed * Time.deltaTime;
			position.x = Mathf.Clamp (position.x, minPos, maxPos);
			transform.position = position; 
		}
		position = transform.position;
		position.x = Mathf.Clamp (position.x, minPos, maxPos);
		transform.position = position;

	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Enemy Car") {
			//Destroy(gameObject);
			gameObject.SetActive(false);
			ui.GameOveActivated();

			am.carSound.Stop ();
		}

	}

	public void MoveLeft(){
		rb.velocity = new Vector2 (-carSpeed, 0);
	}

	public void MoveRight(){
		rb.velocity = new Vector2 (carSpeed, 0);
	}

	public void SetVelocityZero() {
		rb.velocity = Vector2.zero;
	}

	public void TouchMove() {
		if (Input.touchCount > 0) {
			Touch touch = Input.GetTouch (0);
			float middle = Screen.width / 2;

			if (touch.position.x < middle && touch.phase == TouchPhase.Began) {
				MoveLeft ();
			} else if (touch.position.x > middle && touch.phase == TouchPhase.Began) {
				MoveRight ();
			}
		} else {
			SetVelocityZero ();
		}
	}

	void AccelerometerMove() {
		float x = Input.acceleration.x;

		if (x < -0.1f) {
			MoveLeft ();
		} else if (x > 0.1f) {
			MoveRight ();
		} else {
			SetVelocityZero ();
		}


		
	}

}
