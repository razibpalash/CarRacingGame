﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public Text scoreText;
	int score;
	bool gameOver;

	public Button[] buttons;

	// Use this for initialization
	void Start () {
		gameOver = false;
		score = 0;
		InvokeRepeating ("ScoreUpdate", 1f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		scoreText.text = "Score : " + score;

	}

	void ScoreUpdate(){
		if (!gameOver) {			
			score += 3;
		}
	}

	public void GameOveActivated(){
		gameOver = true;
		foreach (Button button in buttons) {
			button.gameObject.SetActive (true);
		}
	}

	public void Play(){
		Application.LoadLevel ("Level");
	}

	public void Pause(){
		if (Time.timeScale == 1) {
			Time.timeScale = 0;
		} else if(Time.timeScale == 0) {
			Time.timeScale = 1;
		}
	}

	public void RePlay(){
		Application.LoadLevel (Application.loadedLevel);
	}

	public void Menu(){
		Application.LoadLevel ("MenuScene");
	}

	public void Exit(){
		Application.Quit ();
	}

}
